FROM jenkins/jenkins:lts

MAINTAINER hino

ENV JENKINS_USER admin
ENV JENKINS_PASS admin

# Skip initial setup
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

# pre-installed plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN ATTEMPTS=3 /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
